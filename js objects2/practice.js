
function Character(name, job, level){
	this.name = name;
	this.job = job;
	this.level = level;
	this.health = 2*level;
	this.attack = level/2;
	this.dodge = level/4;
	this.power = level*4;

	this.battle = function(enemy){
		console.log(this.name+" attacked "+enemy.name);
		enemy.health = enemy.health - this.attack;
		console.log(enemy.name+" health is now reduced to: "+enemy.health);
		if(enemy.health <= 0){
			enemy.faint();
		}
		if(enemy.power > this.power){
			enemy.ability1();
			return;
		}
		else if(enemy.dodge > this.dodge){
			enemy.ability2();
			enemy.health = enemy.health - this.attack*2;
			console.log(enemy.name+" health is now reduced to: "+enemy.health);
			return;
		}
	}
	this.faint = function(){
		console.log(this.name+" fainted");
	}
	this.ability1 = function(){
		console.log(this.name+" dodged the attack, no damage has taken.");
	}
	this.ability2 = function(){
		console.log(this.name+" is so powerful, double damage has taken.");
	}
}

let swordsman = new Character("Zorro", "knight", 200);
let mage = new Character("Nami", "Magician", 50);
let fighter = new Character("Luffy", "Fighter", 500);

console.log(swordsman);
console.log(mage);
console.log(fighter);

swordsman.battle(mage);
fighter.battle(swordsman);
mage.battle(fighter);
