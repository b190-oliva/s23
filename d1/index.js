
// Objects

// - objects are data type that are used to represent real world objects


let cellphone = {
	name: "Nokie 3210",
	manufactureDate: 1999
}
console.log("Result from creating objects using initializers");
console.log(cellphone);
console.log(typeof cellphone);

// let car = {
// 	model: "GTR",
// 	yearReleased: 2010
// };
// console.log(car);

// constructor function
// creates a reusable function/construct several objects that have same data structure

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufacturedDate = manufactureDate;
}

let laptop = new Laptop("Dell", 2012);
console.log("Result from creating objects using initializers");
console.log(laptop);
console.log(typeof laptop);

let laptop2 = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using initializers");
console.log(laptop2);
console.log(typeof laptop2);

let computer = {};
let myComputer = new Object();
console.log("Result from creating objects using initializers");
console.log(myComputer);
console.log(typeof myComputer);

console.log("Result from dot notation: "+laptop.name);

let laptops = [laptop,laptop2];
console.log(laptops[1].name);
console.log(laptops[0].manufacturedDate);

let car = {};
car.name = "Honda Civic"
car.manufacturedDate= 2019;
console.log(car);

car["manufactured date"] = 2019;
console.log(car);

//delete of properties

delete car["manufactured date"];
console.log(car);

// reassigning of properties

car.name = "Dodge Charger R/T";
console.log(car);

let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is "+this.name);
	}
}
console.log(person);
console.log("Result from object methods");
person.talk();

person.walk = function(){
	console.log(`${this.name} walked 25 steps`);
}
person.walk();

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		state: "Texas"
	},
	emails: ['joe@mail.com','johnHandsome@mail.com'],
	introduce: function(){
		console.log("Hello my name is "+this.firstName+" "+ this.lastName);
	} 
}
friend.introduce();
console.log(friend);

// real-world application of objects

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This Pokemon tackled target Pokemon");
		console.log("Target Pokemon's health is now reduced to _targetPokemonHealth_");
	},
	faint: function(){
		console.log("Pokemon Fainted");
	}
};
console.log(myPokemon);

// using constructor function

function Pokemon(name, level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	//methods

	this.tackle = function(target){
		console.log(this.name+" tackled "+ target.name);
		console.log("TargetPokemon's health is now reduced to: _targetPokemonHealth_");
	}
	this.faint = function(){
		console.log(this.name + " fainted");
	}
}

let MegaJirachi = new Pokemon("Mega Jirachi", 20);
let Metaggros = new Pokemon("Metaggros", 50);

//using tackle method from jirachi with metaggros as its target
MegaJirachi.tackle(Metaggros);