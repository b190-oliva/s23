


let book1 = {
	Title: "The Catcher in the Rye",
	Author: "J. D. Salinge",
	DatePublished: '6-15-1951',
	Summary: "The novel details two days in the life of 16-year-old Holden Caulfield after he has been expelled from prep school. Confused and disillusioned, Holden searches for truth and rails against the “phoniness” of the adult world."
};
let book2 = {
	Title: "To Kill a Mockingbird",
	Author: "Harper Lee",
	DatePublished: '6-11-1960',
	Summary: "Set in small-town Alabama, the novel is a bildungsroman, or coming-of-age story, and chronicles the childhood of Scout and Jem Finch as their father Atticus defends a Black man falsely accused of rape. Scout and Jem are mocked by classmates for this."
};
let book3 = {
	Title: "The Great Gatsby", 
	Author: "F. Scott Fitzgerald",
	DatePublished: '4-10-1925',
	Summary: "Set in Jazz Age New York, it tells the tragic story of Jay Gatsby, a self-made millionaire, and his pursuit of Daisy Buchanan, a wealthy young woman whom he loved in his youth."
}

console.log(book1,book2,book3);