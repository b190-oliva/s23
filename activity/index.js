

let trainer = {
	name: "Brock Lesnar",
	age: 21,
	pokemon: ["Ash's Pikachu","Mega Jirachi","Mega Ash-Greninja", "Mega Charizard X"],
	friends:{
		Kanto: ["Misty","Team rocket"],
		Hoenn: ["Silvana","Jiratina"],
	},
	talk: function(choose){
		console.log("Pikachu! I choose you!");
	}
}
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Resultof square bracket notation:");
console.log(trainer.pokemon);
console.log("Result of talk method:");
trainer.talk();



function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level/2;
	this.tackle = function(target){
		console.log(this.name+" tackled "+target.name);
		target.health = target.health-this.attack;
		console.log(target.name+" health is now reduced to: "+target.health);
		if(target.health <= 0){
			target.faint();
		}
		console.log(target);
	}
	this.faint = function(){
		console.log(this.name+ " fainted");
	}
}
let Jirachi = new Pokemon("Jirachi", 30);
let Metagross = new Pokemon("Metagross", 50);
let MegaRayquaza = new Pokemon("Mega Rayquaza", 300);

console.log(Metagross);
console.log(Jirachi);
console.log(MegaRayquaza);

Jirachi.tackle(Metagross);
MegaRayquaza.tackle(Metagross);
MegaRayquaza.tackle(Jirachi);
